// JavaScript source code
// JavaScript source code
var OIS301B_salespriceupdateI = (function () {

    var controller;

    // define controller

    function OIS301B_salespriceupdateI(args) {
        this.scriptName = "OIS301B_salespriceupdateI";
        this.controller = args.controller;
        content = this.controller.GetContentElement();
        controller = this.controller;

        var hocd = 0;

        this.log = args.log;
    }

    //INIT

    OIS301B_salespriceupdateI.Init = function (args) {

        // run function is called by Init routine.
        new OIS301B_salespriceupdateI(args).run();



    };

    // run is called by init and so keeps running and executes everything in its path. 

    OIS301B_salespriceupdateI.prototype.run = function () {
        var _this = this;
        var controller = this.controller;
        var grid = controller.GetGrid();
        if (!grid) {
            return;
        }
        this.grid = grid;
        // Subscribe to the onSelectedRowsChanged event and store the handler for unsubscribe when the user navigates away from the current panel.
        var handler = function (e, args) { _this.onSelectionChanged(e, args); };
        grid.onSelectedRowsChanged.subscribe(handler);
        this.selectionHandler = handler;

        this.unregisterRequested = controller.Requested.On(function (e) {
            _this.onRequested(e);
        });

        this.unsubscribeRequesting = controller.Requesting.On(function (e) {
            _this.onRequesting(e);
        });


        grid.setSelectedRows([]);

        // user and company are retrieved from the local data area.

        this.usid = ScriptUtil.GetUserContext("USID");
        this.cono = ScriptUtil.GetUserContext("CONO");

        this.addElements();
		
	

                                               
		
        

    };

    OIS301B_salespriceupdateI.prototype.addElements = function () {
        var _this = this;
        var run = new ButtonElement();
        run.Name = "run";
        run.Value = "Change Net price";
        run.Position = new PositionElement();
        run.Position.Top = 7;
        run.Position.Left = 85;
        run.Position.Width = 10;

        var contentElement = this.controller.GetContentElement();

        var $run = contentElement.AddElement(run);
        //var $txt = contentElement.AddElement(txt);

        $("#run").hide();


        $run.click({}, function () {




            _this.selectlineDialog();
        });
    };

    OIS301B_salespriceupdateI.prototype.onRequested = function (args) {
        // Unregister all events
        this.unregisterRequested();
        var grid = this.grid;
        if (grid) {
            grid.onSelectedRowsChanged.unsubscribe(this.selectionHandler);
        }



    };

    OIS301B_salespriceupdateI.prototype.onRequesting = function (args) {
        this.log.Info("onRequesting");

    }


    OIS301B_salespriceupdateI.prototype.onSelectionChanged = function (e, args) {
        var _this = this;
        var grid = args.grid;
        var selected = grid.getSelectedRows();

        //following column value(s) is/are captured when row(s) are selected.
        orno = ListControl.ListView.GetValueByColumnName('ORNO');
        ponr = ListControl.ListView.GetValueByColumnName('PONR');
        sapr = ListControl.ListView.GetValueByColumnName('SAPR');
        nepr = ListControl.ListView.GetValueByColumnName('NEPR');
        itds = ListControl.ListView.GetValueByColumnName('TEDS');
        itno = ListControl.ListView.GetValueByColumnName('ITNO');
        dia3 = ListControl.ListView.GetValueByColumnName('DIA3');
        lnam = ListControl.ListView.GetValueByColumnName('LNAM');
        orqt = ListControl.ListView.GetValueByColumnName('ORQT');
		
		 orno = ScriptUtil.GetFieldValue('OAORNO'); // Item number


        $("#run").show();

        //posx = ListControl.ListView.GetValueByColumnName('POSX');

        console.log(orno + "orderno");
        console.log(ponr + "lineno");
        console.log(sapr + "listprice");
        console.log(nepr + "netprice");
		

		


    };
	
	
	
	


    // pop up message function appears when either now row selected or no Price entered.
    OIS301B_salespriceupdateI.prototype.selectlineDialog = function () {
        var _this = this;


        //First add the html to the page..
        $("body").append('<div id="dialog1" style="display:none;"><br/ > <p id=itno></p> <p id=itds></p> <p id=orqt></p>   <p id=nepr></p> <p id=lnam></p> <p>New net price:</p> <input type=text id="slsprice" class="inforTextBox" /> <br /> <p id=errormsg></p>  </div>');

        $("#itno").append("<p>itemno: " + itno + "</p>");
        $("#itds").append("<p>name: " + itds + "</p>");
        $("#orqt").append("<p>ordered qty: " + orqt + "</p>");
        $("#nepr").append("<p>current net price = " + nepr + "</p>");
        $("#lnam").append("<p>current line amount = " + lnam + "</p>");

        //Invoke the dialog on it
        $('#dialog1').inforMessageDialog({
            title: "Change Net Price",
            dialogType: "General",
            width: 500,
            height: 250,
            close: function (event, ui) {
                $(this).remove();
            },
            buttons: [{
                text: "Ok",
                click: function () {

                    //this is the new net price that is entered
                    salesprice = $("#slsprice").val();
                    console.log("that's right");

                    $("#errormsg").empty();

                    debugger;
					
						var debug= '';

		debugger;
		
		  $.ajax(
                                                    {
                                                        url: '/m3api-rest/execute/CMS100MI/Lst_Discount01?OBORNO=' + orno,
                                                        dataType: 'json',
                                                        async: false,
                                                        success: function (miResult) { get_mandisc(miResult, debug); }
                                                    }
                                                )
					
					 function get_mandisc(result, debug) {
						 debugger;
                                                    var miRecords = result.MIRecord;

                                                    if (null != miRecords) {
                                                        for (var i = 0; i < miRecords.length; i++) {

														debugger;
                                                            var nameValue = miRecords[i].NameValue;
															
															
															
                                                           dia3 = parseFloat(nameValue[3].Value);
                                                        }


                                                    }
                                                }
					
					
					

                    console.log(salesprice + "this is the new net price");

                    console.log(nepr + "this is the old net price");

					debugger;
					
                    console.log(dia3 + "is xero");

                    if (dia3 == "") { dia3 = "0" };

                    Odia3 = parseFloat(dia3);


                    dis03 = (nepr - salesprice + Odia3);
                    console.log(dis03 + "additional discount written");

                    dis04 = dis03.toFixed(2);


                    console.log(dis04 + "this is the discount 03 that will be written");

                   


                  

                        SessionCache.Add('prices', dis04);

                        $(this).inforDialog("close");


                        console.log("price")
                        controller.ListOption("2");




                        //
                  
                    //


                }
            }, {
                text: "Cancel",
                click: function () {

                    $("#dialog1").inforDialog("close");
                }, isDefault: true
            }]
        });


    };


    return OIS301B_salespriceupdateI;

}());